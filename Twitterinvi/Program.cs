﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
using System.Text;

using System.Configuration;

using Tweetinvi;
using Tweetinvi.Models;
using System.Globalization;

namespace Twitterinvi
{
    class Program
    {
        // Config Settings
        public static string TWITTER_HANDLE
        {
            get
            {
                return ConfigurationSettings.AppSettings["TWITTER_HANDLE"];
            }
        }
        public static string CONSUMER_KEY
        {
            get
            {
                return ConfigurationSettings.AppSettings["CONSUMER_KEY"];
            }
        }
        public static string CONSUMER_SECRET
        {
            get
            {
                return ConfigurationSettings.AppSettings["CONSUMER_SECRET"];
            }
        }
        public static string ACCESS_TOKEN
        {
            get
            {
                return ConfigurationSettings.AppSettings["ACCESS_TOKEN"];
            }
        }
        public static string ACCESS_TOKEN_SECRET
        {
            get
            {
                return ConfigurationSettings.AppSettings["ACCESS_TOKEN_SECRET"];
            }
        }

        public static bool Run_QueryTwitterFollowersContest
        {
            get
            {
                var value = ConfigurationSettings.AppSettings["RUN_QueryTwitterFollowersContest"];

                return Convert.ToBoolean(value);
            }
        }

        public static bool Run_TwitterBot_Unfollower_NonFollowers
        {
            get
            {
                var value = ConfigurationSettings.AppSettings["RUN_TwitterBot_Unfollower_NonFollowers"];

                return Convert.ToBoolean(value);
            }
        }
        public static int MaxToUnfollow
        {
            get
            {
                var value = ConfigurationSettings.AppSettings["MaxToUnfollow"];

                return Convert.ToInt32(value);
            }
        }

        public static int DaysBeforeUnfollow
        {
            get
            {
                var value = ConfigurationSettings.AppSettings["DaysBeforeUnfollow"];

                return Convert.ToInt32(value);
            }
        }

        public static bool Run_TwitterBot_Follow
        {
            get
            {
                var value = ConfigurationSettings.AppSettings["RUN_TwitterBot_Follow"];

                return Convert.ToBoolean(value);
            }
        }
        public static int MaxToFollow
        {
            get
            {
                var value = ConfigurationSettings.AppSettings["MaxToFollow"];
                return Convert.ToInt32(value);
            }
        }
        public static string TwitterSearchQuery
        {
            get
            {
                int count = Convert.ToInt32( ConfigurationSettings.AppSettings["SearchQuery_Count"] );
				int day = DateTime.Now.DayOfYear % count;
				var value = ConfigurationSettings.AppSettings["SearchQuery_" + day.ToString()];
                return Convert.ToString(value);
            }
        }

        public static string LogFileName
        {
            get
            {
                if(String.IsNullOrEmpty(logFileName))
                {
                    logFileName = ConfigurationSettings.AppSettings["LogFileName"];
                }

                return logFileName;
            }
        }
        private static string logFileName = String.Empty;

        public static string DATA_DIRECTORY = "Data";
        public static string DATA_TWITTERFOLLOWED = DATA_DIRECTORY + "/TwitterFollowed.csv";
        public static string DATA_TWITTERUNFOLLOWED = DATA_DIRECTORY + "/TwitterUnFollowed.csv";

        static void Main(string[] args)
        {
            // Ensure DATA files exist
            if (!Directory.Exists(DATA_DIRECTORY))
            {
                Directory.CreateDirectory(DATA_DIRECTORY);
            }
            if (!File.Exists(DATA_TWITTERFOLLOWED))
            {
                File.Create(DATA_TWITTERFOLLOWED);
            }
            if (!File.Exists(DATA_TWITTERUNFOLLOWED))
            {
                File.Create(DATA_TWITTERUNFOLLOWED);
            }

            if(Run_TwitterBot_Unfollower_NonFollowers)
            {
                TwitterBot_Unfollower_NonFollowers();
            }

            if(Run_TwitterBot_Follow)
            {
                TwitterBot_Follow();
            }
        }

        public static void TwitterBot_Follow()
        {
            int MAX_FOLLOW_PER_DAY = MaxToFollow;

            Log("TwitterBot_Follow : BEGIN");

            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

            var user = User.GetUserFromScreenName(TWITTER_HANDLE);
            if (user == null)
            {
                Log("Invalid account credentials! ABORTING");
                return;
            }

            // Don't follow back anybody you recently unfollowed
            List<long> recentlyUnfollowed = RecentlyUnFollowed();

            // Don't follow anybody you already follow
            List<long> currentlyFollowing = new List<long>();
            foreach (long uId in user.GetFriendIds())
            {
                currentlyFollowing.Add(uId);
            }

            // 'Friends' = Following
            Log(string.Format("{0}: Following:{1} - Followers:{2}", user.ScreenName, user.FriendsCount, user.FollowersCount));

            var searchParamGenerator = Search.SearchQueryParameterGenerator;
            var searchTweetParams = searchParamGenerator.CreateSearchTweetParameter(TwitterSearchQuery);
			Log("Following users in query \"" + TwitterSearchQuery + "\"");

            searchTweetParams.Lang = LanguageFilter.English;
            //searchTweetParams.SearchType = SearchResultType.Popular;
            searchTweetParams.MaximumNumberOfResults = 5000;

            var twitterFollowedCSV = new StringBuilder();

            int errorCount = 0;

            // Follow users who post with hashtag #indiedev
            foreach (ITweet t in Search.SearchTweets(searchTweetParams))
            {
                if (t.IsRetweet)
                {
                    continue;
                }

                var targetUser = User.GetUserFromId(t.CreatedBy.Id);

                if (targetUser == null)
                {
                    continue;
                }

                // Didn't recently unfollow & not currently following & Haven't hit the max follow per day
                if (!recentlyUnfollowed.Contains(targetUser.Id) && !currentlyFollowing.Contains(targetUser.Id) && MAX_FOLLOW_PER_DAY > 0)
                {
                    if (targetUser.ScreenName.Equals(TWITTER_HANDLE, StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }

                    // Don't follow accounts with very few followers or who don't follow back + too many tweets (bots)
                    // or who follow more than get followers
//                    float ratio = (float)targetUser.FriendsCount / (float)targetUser.FollowersCount;
                    if (/* ratio > 1.7f ||*/ targetUser.FriendsCount < 100 || targetUser.FollowersCount < 100 || targetUser.StatusesCount > 10000 )
                    {
                        continue;
                    }

                    Log(string.Format("{0}: Follow {1} -> Followers: {2}, Following: {3}, Tweets: {4}", MAX_FOLLOW_PER_DAY, targetUser.ScreenName, targetUser.FollowersCount, targetUser.FriendsCount, targetUser.StatusesCount));

                    /*
                     * Twitterinvi - API: FollowUser
                     */
                    bool success = true;
                    if (User.FollowUser(targetUser.Id) == false)
                    {
                        errorCount++;
                        Thread.Sleep(1000);
                        if (User.FollowUser(targetUser.Id) == false) // try again
                        {
                            Log("  TwitterBot - ERROR #" + errorCount + " FOLLOWING");
                            if (errorCount > 2)
                            {
                                Log("  TwitterBot - Aborting due to too many errors.");
                                break;
                            }
                            success = false;
                        }
                    }

                    if (success)
                    {
                        currentlyFollowing.Add(targetUser.Id);
                        MAX_FOLLOW_PER_DAY--;

                        //[TwitterFollowed] ([timestamp] ,[twitterhandle] ,[twitterid]) VALUES(GETDATE(), @twitterhandle, @twitterid)"
                        twitterFollowedCSV.AppendLine(string.Format("{0}, {1}, {2}", DateTime.UtcNow, targetUser.ScreenName, targetUser.Id));
                    }
                    // Sleep for 1 second
                    Thread.Sleep(1000);
                }
            }

            File.AppendAllText(DATA_TWITTERFOLLOWED, twitterFollowedCSV.ToString());

            Log("TwitterBot_Follow : COMPLETED");
        }


        public static void TwitterBot_Unfollower_NonFollowers()
        {
            int MAX_UNFOLLOW_PER_DAY = MaxToUnfollow;

            Log("TwitterBot_Unfollower_NonFollowers : BEGIN");

            Auth.SetUserCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);

            var myTwitterAccount = User.GetUserFromScreenName(TWITTER_HANDLE);
            if (myTwitterAccount == null)
            {
                Log("Invalid account credentials! ABORTING");
                return;
            }

            // 'Friends' = Following
            Log(string.Format("{0}: Following:{1} - Followers:{2}", myTwitterAccount.ScreenName, myTwitterAccount.FriendsCount, myTwitterAccount.FollowersCount));

            List<long> followersId = new List<long>();
            foreach (long uId in myTwitterAccount.GetFollowerIds())
            {
                followersId.Add(uId);
            }

            List<long> followingId = new List<long>();
            foreach (long uId in myTwitterAccount.GetFriendIds())
            {
                followingId.Add(uId);
            }

            List<long> recentlyFollowed = RecentlyFollowed(DaysBeforeUnfollow);

            var twitterUnfollowedCSV = new StringBuilder();

            // Unfollow those who don't follow back
            foreach (long uId in followingId)
            {
                // Does not follow me & was not recently followed
                if (!followersId.Contains(uId) && !recentlyFollowed.Contains(uId))
                {
                    string userHandle = User.GetUserFromId(uId).ScreenName;

                    Log(string.Format("{0} Unfollowed: {1}", MAX_UNFOLLOW_PER_DAY, userHandle));

                    /*
                     * Twitterinvi - API: UnFollowUser
                     */
                    User.UnFollowUser(uId);

                    //[TwitterUnfollowed] ([timestamp] ,[twitterhandle] ,[twitterid]) VALUES(GETDATE(), @twitterhandle, @twitterid)
                    twitterUnfollowedCSV.AppendLine(string.Format("{0}, {1}, {2}", DateTime.UtcNow, userHandle, uId));

                    MAX_UNFOLLOW_PER_DAY--;

                    if (MAX_UNFOLLOW_PER_DAY <= 0)
                    {
                        Log("MAX Unfollow reached.");
                        break;
                    }

                    // Sleep for 2 seconds
                    Thread.Sleep(2000);
                }
            }

            File.AppendAllText(DATA_TWITTERUNFOLLOWED, twitterUnfollowedCSV.ToString());

            Log("TwitterBot_Unfollower_NonFollowers : COMPLETED");
        }

        // Helper Methods
        private static List<long> AllFollowedInDatabase()
        {
            List<long> allFollowed = new List<long>();

            // SELECT DISTINCT [twitterid] FROM [TwitterBot].[dbo].[TwitterFollowed]

            // Read
            using (var sr = File.OpenText(DATA_TWITTERFOLLOWED))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var fields = line.Split(',');
                    var twitteridRaw = fields[2].Trim();

                    long twitterid = Convert.ToInt64(twitteridRaw);

                    if (!allFollowed.Contains(twitterid))
                    {
                        allFollowed.Add(twitterid);
                    }
                }
            }

            return allFollowed;
        }

        private static List<long> RecentlyFollowed(int daysBeforeUnfollow)
        {
            List<long> recentlyFollowed = new List<long>();
            DateTime cutoff = DateTime.UtcNow.AddDays(-daysBeforeUnfollow);

            // SELECT DISTINCT [twitterid], [timestamp] FROM [TwitterBot].[dbo].[TwitterFollowed] WHERE [timestamp] >= DATEADD(day,-{0}, GETDATE())", daysBeforeUnfollow

            // Read
            using (var sr = File.OpenText(DATA_TWITTERFOLLOWED))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var fields = line.Split(',');
                    var dateRaw = fields[0].Trim();
                    var twitteridRaw = fields[2].Trim();

                    long twitterid = Convert.ToInt64(twitteridRaw);

                    // Filter by date

                    DateTime date = DateTime.Parse(dateRaw, CultureInfo.CurrentCulture, DateTimeStyles.AssumeUniversal );
//                    DateTime date = Convert.ToDateTime(dateRaw);

                    if(date >= cutoff && !recentlyFollowed.Contains(twitterid))
                    {
                        recentlyFollowed.Add(twitterid);
                    }
                }
            }

            return recentlyFollowed;
        }


        private static List<long> RecentlyUnFollowed()
        {
            List<long> recentlyUnFollowed = new List<long>();

            // SELECT DISTINCT [twitterid] FROM [TwitterBot].[dbo].[TwitterUnfollowed]

            // Read
            using (var sr = File.OpenText(DATA_TWITTERUNFOLLOWED))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    var fields = line.Split(',');
                    var twitteridRaw = fields[2].Trim();

                    long twitterid = Convert.ToInt64(twitteridRaw);

                    if (!recentlyUnFollowed.Contains(twitterid))
                    {
                        recentlyUnFollowed.Add(twitterid);
                    }
                }
            }

            return recentlyUnFollowed;
        }

        private static void Log(string log)
        {
            string logTimestamped = DateTime.Now + " : " + log;

            // Create new file if needed
            if (!File.Exists(LogFileName))
            {
                // Create a file to write to.
                using (StreamWriter sw = File.CreateText(LogFileName))
                {
                    sw.WriteLine(logTimestamped);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(LogFileName))
                {
                    sw.WriteLine(logTimestamped);
                }
            }

            Console.WriteLine(logTimestamped);
        }
    }
}
